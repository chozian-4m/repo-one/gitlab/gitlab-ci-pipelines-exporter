ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/dsop/redhat/ubi/ubi8
ARG BASE_TAG=8.5


FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY gitlab-ci-pipelines-exporter_v0.5.3_linux_amd64.rpm /tmp/

#RUN tar zxvf gitlab-ci-pipelines-exporter_v0.5.2_linux_386.tar.gz -C /usr/local/bin

RUN dnf update -y && \
    rpm -ivh --nodigest --nofiledigest /tmp/gitlab-ci-pipelines-exporter_v0.5.3_linux_amd64.rpm && \
    rm -rf /tmp/gitlab-ci-pipelines-exporter_v0.5.3_linux_amd64.rpm && \
    dnf clean all && \
    rm -rf /var/cache/dnf 


USER 65534

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/gitlab-ci-pipelines-exporter"]
CMD [""]
HEALTHCHECK NONE
