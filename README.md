# <application name>
🦊 gitlab-ci-pipelines-exporter
PkgGoDev Go Report Card Docker Pulls test Coverage Status release gitlab-ci-pipelines-exporter

gitlab-ci-pipelines-exporter allows you to monitor your GitLab CI pipelines with Prometheus or any monitoring solution supporting the OpenMetrics format.

You can find more information on GitLab docs about how it takes part improving your pipeline efficiency.

